package org.matheclipse.core.examples;

import static org.matheclipse.core.expression.F.*;

import org.matheclipse.core.eval.ExprEvaluator;
import org.matheclipse.core.eval.util.IAssumptions;
import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.parser.client.SyntaxError;
import org.matheclipse.parser.client.math.MathException;

public class AssumptionsExample {

	public static void main(String[] args) {
		try {
			ExprEvaluator util = new ExprEvaluator();

			// define Assumptions: x > 0 && Element(y, Integers)
			IAssumptions assumptions = org.matheclipse.core.eval.util.Assumptions.getInstance(And(Greater(x, C0),
					Element(y, Integers)));
			util.getEvalEngine().setAssumptions(assumptions);

			IAST function = Abs(x);
			IExpr result = util.evaluate(function);
			// print: x
			System.out.println(result.toString());

			function = Abs(y);
			result = util.evaluate(function);
			// print: Abs(y)
			System.out.println(result.toString());

			function = Floor(x);
			result = util.evaluate(function);
			// print: Floor(x)
			System.out.println(result.toString());

			function = Floor(y);
			result = util.evaluate(function);
			// print: y
			System.out.println(result.toString());

			// use Refine() to evaluate an expression directly with assumptions
			// Show an expression in the Java form:
			String javaForm = util.toJavaForm("Refine(Abs(n+1), n>=0)");
			// prints: Refine(Abs(Plus(n,C1)),GreaterEqual(n,C0))
			System.out.println(javaForm.toString());

			function = Refine(Abs(Plus(n, C1)), GreaterEqual(n, C0));
			result = util.evaluate(function);
			// prints: 1+n
			System.out.println(result.toString());
			
		} catch (SyntaxError e) {
			// catch Symja parser errors here
			System.out.println(e.getMessage());
		} catch (MathException me) {
			// catch Symja math errors here
			System.out.println(me.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
