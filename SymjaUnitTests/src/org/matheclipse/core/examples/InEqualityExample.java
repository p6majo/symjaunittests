package org.matheclipse.core.examples;

import static org.matheclipse.core.expression.F.Less;
import static org.matheclipse.core.expression.F.Plus;
import static org.matheclipse.core.expression.F.Times;
import static org.matheclipse.core.expression.F.integer;
import static org.matheclipse.core.expression.F.x;

import org.matheclipse.core.eval.ExprEvaluator;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.parser.client.SyntaxError;
import org.matheclipse.parser.client.math.MathException;

public class InEqualityExample {
	public static void main(String[] args) {
		try {
			ExprEvaluator util = new ExprEvaluator();
			IExpr result = util.evaluate("2*x + 4 < 10");
			// print: x < 3
			System.out.println(result.toString());

			// Create an expression with F.* static methods:
			IExpr function = Less(Plus(Times(integer(2), x), integer(4)), integer(10));
			result = util.evaluate(function);
			// print: x < 3
			System.out.println(result.toString());

		} catch (SyntaxError e) {
			// catch Symja parser errors here
			System.out.println(e.getMessage());
		} catch (MathException me) {
			// catch Symja math errors here
			System.out.println(me.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
