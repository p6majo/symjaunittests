package org.matheclipse.core.examples;

import org.matheclipse.core.basic.Config;
import org.matheclipse.core.eval.ExprEvaluator;
import org.matheclipse.core.graphics.Show2SVG;
import org.matheclipse.core.interfaces.IAST;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.parser.client.SyntaxError;
import org.matheclipse.parser.client.math.MathException;

public class PlotExample {
	public static void main(String[] args) {
		try {
			// don't distinguish between lower- and uppercase identifiers
			Config.PARSER_USE_LOWERCASE_SYMBOLS = true;
			ExprEvaluator util = new ExprEvaluator(false, 100);

			// plot a simple function
			IExpr result = util.evaluate("Plot(sin(x),{x,-10,10})");

			if (result.isAST()) {
				StringBuilder svgData = new StringBuilder();
				Show2SVG.toSVG((IAST) result, svgData);
				System.out.println(svgData.toString());
			}
		} catch (SyntaxError e) {
			// catch Symja parser errors here
			System.out.println(e.getMessage());
		} catch (MathException me) {
			// catch Symja math errors here
			System.out.println(me.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
