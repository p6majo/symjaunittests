package org.matheclipse.core.examples;

import org.matheclipse.core.eval.ExprEvaluator;
import org.matheclipse.core.interfaces.IExpr;
import org.matheclipse.parser.client.SyntaxError;
import org.matheclipse.parser.client.math.MathException;

/**
 * Example for the <a href="https://bitbucket.org/axelclk/symja_android_library/wiki/Home">Symja Solve() function.</a>
 *
 */
public class SO_33586896_SolveExample {
	public static void main(String[] args) {
		try {
			ExprEvaluator util = new ExprEvaluator(false, 100);

			// Solve linear equations:
			IExpr result = util.evaluate("Solve({10-x+y ==5, 4*x == 8y} ,{x,y})");
			// print: {{x->10,y->5}}
			System.out.println(result.toString());
 
		} catch (SyntaxError e) {
			// catch Symja parser errors here
			System.out.println(e.getMessage());
		} catch (MathException me) {
			// catch Symja math errors here
			System.out.println(me.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
