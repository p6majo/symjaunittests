package org.matheclipse.core.examples;

import java.io.StringWriter;

import org.matheclipse.core.basic.Config;
import org.matheclipse.core.eval.EvalEngine;
import org.matheclipse.core.eval.TeXUtilities;
import org.matheclipse.core.expression.F;
import org.matheclipse.parser.client.SyntaxError;
import org.matheclipse.parser.client.math.MathException;

public class TeXExample {
	public static void main(String[] args) {
		try {
			// false -> distinguish between upper- and lowercase identifiers:
			Config.PARSER_USE_LOWERCASE_SYMBOLS = false;
			F.initSymbols(null, null, false);
			// false -> switch to Mathematica syntax mode:
			EvalEngine engine = new EvalEngine(false);
			//
			TeXUtilities texUtil = new TeXUtilities(engine, false);

			StringWriter stw = new StringWriter();
			texUtil.toTeX("Sum[i, {i,1,n}]", stw);
			// print: \sum_{i = 1}^{n}i
			System.out.println(stw.toString());

			stw = new StringWriter();
			texUtil.toTeX("MatrixForm[{{a,b},{c,d}}]", stw);
			// print:
			// \begin{pmatrix} a & b \\
			// c & d \\
			// \end{pmatrix}
			System.out.println(stw.toString());

		} catch (SyntaxError e) {
			// catch Symja parser errors here
			System.out.println(e.getMessage());
		} catch (MathException me) {
			// catch Symja math errors here
			System.out.println(me.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
